package ru.fadeev.tm.command;

import ru.fadeev.tm.service.CrudAble;

public class ClearCommand  implements Command{
    @Override
    public void execute(CrudAble service) {
        service.clear();
    }
}
