package ru.fadeev.tm.command;

import ru.fadeev.tm.service.CrudAble;

public class ListCommand implements Command {

    @Override
    public void execute(CrudAble service) {
       service.list();
    }
}
