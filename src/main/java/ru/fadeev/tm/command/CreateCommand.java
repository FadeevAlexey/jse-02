package ru.fadeev.tm.command;

import ru.fadeev.tm.service.CrudAble;

public class CreateCommand implements Command {

    @Override
    public void execute(CrudAble service) {
        service.create();
    }
}
