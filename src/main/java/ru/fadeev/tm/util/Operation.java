package ru.fadeev.tm.util;

public enum Operation {
    CREATE,
    CLEAR,
    LIST,
    REMOVE,
    HELP;

    public static Operation getAllowableOperation(String operation) {
        switch (operation.toLowerCase()) {
            case "create":
                return Operation.CREATE;
            case "clear":
                return Operation.CLEAR;
            case "list":
                return Operation.LIST;
            case "remove":
                return Operation.REMOVE;
            case "help":
                return Operation.HELP;
            default:
                throw new IllegalArgumentException("wrong command name");
        }
    }
}
