package ru.fadeev.tm.service;

public interface CrudAble {
   void create();
   void list();
   void clear();
   void remove();
}
