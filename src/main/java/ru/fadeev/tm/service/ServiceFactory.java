package ru.fadeev.tm.service;

import java.util.HashMap;
import java.util.Map;

public class ServiceFactory {
    public Map<String, CrudAble> map = new HashMap<>();
     {
        map.put("project", new ProjectService());
        map.put("task", new TaskService());
    }

    public CrudAble getServiceByName(String nameOfService) {
        if (nameOfService == null || nameOfService.isEmpty())
            return null;
        return map.get(nameOfService);
    }
}




